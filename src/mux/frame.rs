use bytes::{Buf, BufMut, BytesMut};

use crate::StreamId;

pub struct Frame {
    command: Command,
    stream_id: StreamId,
    length: u16,
    body: Option<BytesMut>,
}

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq)]
#[repr(u8)]
pub enum Command {
    OPEN,
    PUSH,
    NOOP,
    CLOSE,
}

impl Frame {
    fn new(command: Command, stream_id: StreamId, body: Option<BytesMut>) -> Self {
        let length = match &body {
            Some(bytes) => bytes.len() as u16,
            None => 0,
        };

        Self {
            command,
            stream_id,
            length,
            body,
        }
    }

    pub fn id(&self) -> StreamId {
        self.stream_id
    }

    pub fn command(&self) -> &Command {
        &self.command
    }

    pub fn body(self) -> Option<BytesMut> {
        self.body
    }

    pub fn open(stream_id: StreamId) -> Self {
        Frame::new(Command::OPEN, stream_id, None)
    }

    pub fn push(stream_id: StreamId, body: BytesMut) -> Self {
        Frame::new(Command::PUSH, stream_id, Some(body))
    }

    pub fn noop(stream_id: StreamId) -> Self {
        Frame::new(Command::NOOP, stream_id, None)
    }

    pub fn close(stream_id: StreamId) -> Self {
        Frame::new(Command::CLOSE, stream_id, None)
    }

    pub fn encode(&self) -> BytesMut {
        let mut bytes = BytesMut::with_capacity(5 + self.length as usize);
        bytes.put_u8(self.command as u8);
        bytes.put_u16(self.stream_id);
        bytes.put_u16(self.length);

        if self.length > 0 {
            bytes.put_slice(&self.body.as_ref().expect("Frame assumed to have body.")[0..(self.length as usize)]);
        }

        bytes
    }

    pub fn decode(mut bytes: BytesMut) -> Self {
        let command = match bytes.get_u8() {
            0 => Command::OPEN,
            1 => Command::PUSH,
            2 => Command::NOOP,
            3 => Command::CLOSE,
            _ => unimplemented!()
        };
        let stream_id = bytes.get_u16();
        let length = bytes.get_u16();
        let body = if length > 0 {
            Some(bytes.split_to(length as usize))
        } else {
            None
        };

        Self {
            command,
            stream_id,
            length,
            body,
        }
    }
}

#[cfg(test)]
mod tests {
    use bytes::Buf;

    use super::*;

    #[test]
    pub fn encode() {
        let frame = Frame::push(2, BytesMut::from("abc"));
        let mut frame_bytes = frame.encode();

        assert_eq!(0x01, frame_bytes.get_u8());
        assert_eq!(0x02, frame_bytes.get_u16());
        assert_eq!(0x03, frame_bytes.get_u16());
        assert_eq!(&[b'a', b'b', b'c'], &frame_bytes[..]);
    }

    #[test]
    pub fn decode() {
        let frame = Frame::push(2, BytesMut::from("abc"));
        let frame_bytes = frame.encode();
        let frame_decoded = Frame::decode(frame_bytes);
        assert_eq!(frame.command, frame_decoded.command);
        assert_eq!(frame.stream_id, frame_decoded.stream_id);
        assert_eq!(frame.length, frame_decoded.length);
        assert_eq!(frame.body, frame_decoded.body);
    }
}