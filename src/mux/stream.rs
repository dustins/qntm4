use std::sync::atomic::{AtomicU16, Ordering};

use async_std::pin::Pin;
use async_std::sync::Receiver;
use bytes::BytesMut;
use futures::task::{Context, Poll};
use pin_project::pin_project;

pub type StreamId = u16;

static NEXT_STREAM_ID: AtomicU16 = AtomicU16::new(1);

#[pin_project]
#[derive(Clone)]
pub struct Stream {
    id: StreamId,

    #[pin]
    receiver: Receiver<BytesMut>,
}

impl Stream {
    pub fn new(id: StreamId, receiver: Receiver<BytesMut>) -> Self {
        Self {
            id,
            receiver,
        }
    }

    // todo replace with StreamManager
    pub fn next_id() -> StreamId {
        NEXT_STREAM_ID.fetch_add(1, Ordering::SeqCst)
    }

    pub fn id(&self) -> StreamId {
        self.id
    }
}

impl futures::stream::Stream for Stream {
    type Item = BytesMut;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        self.project().receiver.poll_next(cx)
    }
}