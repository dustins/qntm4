use async_std::net::{SocketAddr, UdpSocket};
use async_std::sync::Sender;
use async_trait::async_trait;
use bytes::BytesMut;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};

use crate::Runnable;

pub struct UdpReader {
    sender: Sender<BytesMut>,
    address: SocketAddr,
    socket_count: usize,
}

impl UdpReader {
    pub fn new(sender: Sender<BytesMut>, address: SocketAddr, socket_count: usize) -> Self {
        Self {
            sender,
            address,
            socket_count,
        }
    }
}

#[async_trait]
impl Runnable for UdpReader {
    async fn run(&mut self) {
        let bind_address = SockAddr::from(self.address);
        let (domain, _) = match self.address {
            SocketAddr::V4(address) => (Domain::ipv4(), SockAddr::from(address)),
            SocketAddr::V6(address) => (Domain::ipv6(), SockAddr::from(address)),
        };

        for _ in 0..self.socket_count {
            let socket = Socket::new(domain, Type::dgram(), Some(Protocol::udp())).expect("Unable to create socket.");
            socket.set_reuse_address(true).expect(&format!("Unable to set SO_REUSEADDR for socket {:?}", &socket));
            socket.set_reuse_port(true).expect(&format!("Unable to set SO_REUSEPORT for socket {:?}", &socket));
            socket.bind(&bind_address).expect(&format!("Unable to connect to {:?} with {:?}", &bind_address, &socket));

            let socket = UdpSocket::from(socket.into_udp_socket());
            let sender = self.sender.clone();

            async_std::task::spawn(async move {
                loop {
                    let mut bytes = BytesMut::with_capacity(8960);
                    unsafe { bytes.set_len(bytes.capacity()) }

                    match socket.recv(&mut bytes).await {
                        Ok(bytes_read) => sender.send(bytes.split_to(bytes_read)).await,
                        Err(_e) => panic!("Error reading from socket.")
                    }
                }
            });
        }
    }
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use async_std::sync::channel;

    use super::*;

    #[async_std::test]
    async fn test_receive() {
        let chunk_size = 8192;
        let chunk_count = 10;
        let (tx, rx) = channel::<BytesMut>(5);
        let mut connection = UdpReader::new(tx, "0.0.0.0:4488".parse::<SocketAddr>().unwrap(), 3);
        let socket = std::net::UdpSocket::bind("0.0.0.0:0").unwrap();
        socket.connect("0.0.0.0:4488".parse::<SocketAddr>().unwrap()).unwrap();
        let handle = async_std::task::spawn(async move {
            connection.run().await;
        });

        // give time for udp_tunnel.run() to start running
        async_std::task::sleep(Duration::from_millis(10)).await;

        // we can send these all before receiving on the socket because they should all be small
        // enough to fit into the socket buffer
        let mut content = BytesMut::with_capacity(chunk_size * chunk_count);
        unsafe { content.set_len(content.capacity()); }
        for content in content.chunks(chunk_size) {
            socket.send(content);
        }

        for x in 0..chunk_count {
            assert_eq!(chunk_size, rx.recv().await.unwrap().len());
        }
        assert!(rx.try_recv().is_err());
    }
}