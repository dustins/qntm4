use async_std::net::{SocketAddr, UdpSocket};
use async_std::sync::{Arc, Receiver};
use async_trait::async_trait;
use bytes::BytesMut;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};

use crate::{Config, Runnable};

pub struct UdpWriter {
    config: Arc<Config>,
    receiver: Receiver<BytesMut>,
    address: SocketAddr,
}

impl UdpWriter {
    pub fn new(config: Arc<Config>, receiver: Receiver<BytesMut>, address: SocketAddr) -> Self {
        Self {
            config,
            receiver,
            address,
        }
    }
}

#[async_trait]
impl Runnable for UdpWriter {
    async fn run(&mut self) {
        let (domain, address) = match self.address {
            SocketAddr::V4(address) => (Domain::ipv4(), SockAddr::from(address)),
            SocketAddr::V6(address) => (Domain::ipv6(), SockAddr::from(address)),
        };

        for _ in 0..self.config.writer_tasks {
            let socket = Socket::new(domain, Type::dgram(), Some(Protocol::udp())).expect("Unable to create socket.");
            socket.set_reuse_address(true).expect(&format!("Unable to set SO_REUSEADDR for socket {:?}", &socket));
            socket.set_reuse_port(true).expect(&format!("Unable to set SO_REUSEPORT for socket {:?}", &socket));

            let bind_address = SockAddr::from("0.0.0.0:0".parse::<SocketAddr>().unwrap());
            socket.bind(&bind_address).expect(&format!("Unable to connect to {:?} with {:?}", &bind_address, &socket));
            socket.connect(&address).expect(&format!("Unable to connect to {:?} with {:?}", &address, &socket));

            let socket = UdpSocket::from(socket.into_udp_socket());
            let receiver = self.receiver.clone();

            async_std::task::spawn(async move {
                while let Ok(bytes) = receiver.recv().await {
                    socket.send(&bytes).await.expect(&format!("Unable to send bytes with socket {:?}", &socket));
                }
            });
        }
    }
}

#[cfg(test)]
mod tests {
    use async_std::sync::channel;
    use bytes::BytesMut;

    use super::*;

    #[async_std::test]
    async fn test_send() {
        let (tx, rx) = channel::<BytesMut>(10);
        let socket = std::net::UdpSocket::bind("0.0.0.0:0").unwrap();
        let config = Config::default();
        let mut connection = UdpWriter::new(Arc::new(config), rx, socket.local_addr().unwrap());
        let handle = async_std::task::spawn(async move {
            connection.run().await;
        });

        // we can send these all before receiving on the socket because they should all be small
        // enough to fit into the socket buffer
        let content = vec!["hello", "world", "man", "dog", "pig", "cat"];
        for content in &content {
            tx.send(BytesMut::from(*content)).await;
        }

        let mut bytes = Vec::with_capacity(10);
        unsafe { bytes.set_len(bytes.capacity()) }
        for x in 0..content.len() {
            assert!(socket.recv(&mut bytes).unwrap() > 0);
        }
    }
}
