#![feature(fn_traits)]

use async_std::sync::Arc;
use async_std::task;
use async_trait::async_trait;

pub use bench::*;
pub use file::*;
pub use mux::*;

pub use crate::config::{Config, Target};
use crate::reader::Reader;
use crate::writer::Writer;

mod tcp;

mod writer;

mod file;

mod mux;

mod bench;

mod error;
mod config;
mod reader;
mod udp_tunnel;

pub async fn run(config: Arc<Config>) {
    let mut reader = Reader::new(config.clone());
    let streams = reader.output();
    task::spawn(async move {
        reader.run().await;
    });

    let mut writer = Writer::new(config.clone(), streams);
    task::spawn(async move {
        writer.run().await;
    }).await;

    log::info!("Done");
}

#[async_trait]
pub trait Runnable {
    async fn run(&mut self);
}