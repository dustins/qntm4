use std::error::Error;
use std::fmt::{Display, Formatter, Result};

#[derive(Debug)]
pub struct QntmError {
    message: String
}

impl QntmError {
    pub fn new(message: &str) -> Self {
        QntmError {
            message: String::from(message)
        }
    }
}

impl Error for QntmError {}

impl Display for QntmError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{}", self.message)
    }
}