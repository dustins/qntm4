use async_std::net::{SocketAddr, TcpStream};
use async_std::sync::Arc;
use async_trait::async_trait;
use futures::{AsyncWriteExt, StreamExt};

use crate::{Config, mux, Runnable};

pub struct TcpWriter {
    config: Arc<Config>,
    stream: mux::Stream,
    address: SocketAddr,
}

impl TcpWriter {
    pub fn new(config: Arc<Config>, stream: mux::Stream, address: SocketAddr) -> Self {
        Self {
            config,
            stream,
            address,
        }
    }
}

#[async_trait]
impl Runnable for TcpWriter {
    async fn run(&mut self) {
        let mut connection = TcpStream::connect(self.address).await.expect("Unable to connect to socket.");
        let mut total_bytes = 0;

        while let Some(output) = self.stream.next().await {
            match connection.write(&output).await {
                Ok(bytes_writen) => {
                    total_bytes += bytes_writen;
                }
                Err(e) => panic!("Error writing. {:?}", e)
            }
        }

        if self.config.debug {
            log::debug!("[Stream#{}] Sent {} bytes", self.stream.id(), total_bytes);
        }
    }
}