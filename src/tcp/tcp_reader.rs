use async_std::net::{SocketAddr, TcpListener};
use async_std::sync::{Arc, Sender};
use async_std::task;
use async_trait::async_trait;
use bytes::BytesMut;
use futures::AsyncReadExt;

use crate::{Config, mux, Runnable};

pub struct TcpReader {
    config: Arc<Config>,
    tx: Sender<BytesMut>,
    address: SocketAddr,
}

impl TcpReader {
    pub fn new(config: Arc<Config>, tx: Sender<BytesMut>, address: SocketAddr) -> Self {
        Self {
            config,
            tx,
            address,
        }
    }
}

#[async_trait]
impl Runnable for TcpReader {
    async fn run(&mut self) {
        let listener = TcpListener::bind(&self.address).await.expect(&format!("Unable to bind to {:?}", &self.address));
        while let Ok((mut connection, _)) = listener.accept().await {
            let config = self.config.clone();
            let tx_bytes = self.tx.clone();
            task::spawn(async move {
                let stream_id = mux::Stream::next_id();
                tx_bytes.send(mux::Frame::open(stream_id).encode()).await;

                loop {
                    let mut bytes = BytesMut::with_capacity(config.packet_size);
                    unsafe { bytes.set_len(bytes.capacity()) }
                    match connection.read(&mut bytes).await {
                        Ok(0) => break,
                        Ok(bytes_read) => {
                            unsafe { bytes.set_len(bytes_read) }
                            tx_bytes.send(mux::Frame::push(stream_id, bytes).encode()).await;
                        }
                        Err(e) => panic!(e)
                    }
                }

                tx_bytes.send(mux::Frame::close(stream_id).encode()).await;
            });
        }
    }
}