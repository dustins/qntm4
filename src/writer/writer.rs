use std::path::PathBuf;
use std::time::Instant;

use async_std::net::SocketAddr;
use async_std::sync::{Arc, channel, Receiver};
use async_std::task;
use async_trait::async_trait;
use futures::StreamExt;

use crate::{BenchWriter, Config, FileWriter, mux, Runnable, Target};
use crate::tcp::TcpWriter;
use crate::udp_tunnel::UdpWriter;

pub struct Writer {
    config: Arc<Config>,

    streams: Receiver<mux::Stream>,
}

impl Writer {
    pub fn new(config: Arc<Config>, streams: Receiver<mux::Stream>) -> Self {
        Self {
            config,
            streams,
        }
    }
}

#[async_trait]
impl Runnable for Writer {
    async fn run(&mut self) {
        let streams = self.streams.clone();
        let config = self.config.clone();

        match &self.config.destination {
            Target::Bench(total_bytes) => Writer::run_bench(config, streams, *total_bytes).await,
            Target::File(path) => Writer::run_file(config, streams, path.clone()).await,
            Target::TCP(address) => Writer::run_tcp(config, streams, *address).await,
            Target::UDP(address) => Writer::run_udp(config, streams, *address).await,
        }
    }
}

impl Writer {
    async fn writer_per_stream<F: Fn(mux::Stream) -> Box<dyn Runnable + Send>>(config: Arc<Config>, streams: Receiver<mux::Stream>, f: F) {
        while let Ok(stream) = streams.recv().await {
            let stream_id = stream.id();
            let mut writer = f(stream);

            let start = match config.debug {
                true => {
                    log::debug!("[Stream#{}] Starting to write stream", stream_id);
                    Some(Instant::now())
                }
                _ => None
            };

            task::spawn(async move {
                writer.run().await;

                if let Some(start) = start {
                    log::debug!("[Stream#{}] Completed in {:?}", stream_id, start.elapsed());
                }
            });
        }
    }

    async fn run_bench(config: Arc<Config>, streams: Receiver<mux::Stream>, total_bytes: usize) {
        Writer::writer_per_stream(config.clone(), streams, |stream| {
            let writer = BenchWriter::new(config.clone(), stream, total_bytes);
            Box::new(writer)
        }).await;
    }

    async fn run_file(config: Arc<Config>, streams: Receiver<mux::Stream>, path: PathBuf) {
        Writer::writer_per_stream(config.clone(), streams, |stream| {
            let writer = FileWriter::new(config.clone(), stream, path.clone());
            Box::new(writer)
        }).await;
    }

    async fn run_tcp(config: Arc<Config>, streams: Receiver<mux::Stream>, address: SocketAddr) {
        Writer::writer_per_stream(config.clone(), streams, |stream| {
            let writer = TcpWriter::new(config.clone(), stream, address.clone());
            Box::new(writer)
        }).await;
    }

    async fn run_udp(config: Arc<Config>, streams: Receiver<mux::Stream>, address: SocketAddr) {
        let (tx, rx) = channel(32);
        let mut writer = UdpWriter::new(config.clone(), rx.clone(), address.clone());
        let write_handle = task::spawn(async move {
            writer.run().await
        });

        while let Ok(mut stream) = streams.recv().await {
            let tx = tx.clone();
            let config = config.clone();
            task::spawn(async move {
                let start = match config.debug {
                    true => {
                        log::debug!("[Stream#{}] Starting to write stream", stream.id());
                        Some(Instant::now())
                    }
                    _ => None
                };

                while let Some(bytes) = stream.next().await {
                    tx.send(bytes).await;
                }

                if let Some(start) = start {
                    log::debug!("[Stream#{}] Completed in {:?}", stream.id(), start.elapsed());
                }
            });
        }

        write_handle.await;
    }
}