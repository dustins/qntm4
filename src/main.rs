use async_std::sync::Arc;
use simplelog::{CombinedLogger, LevelFilter, SharedLogger, TerminalMode, TermLogger};
use structopt::StructOpt;

use qntm4::{Config, run};

#[async_std::main]
async fn main() {
    let config: Arc<Config> = Arc::new(Config::from_args());

    // create loggers vec
    let mut loggers: Vec<Box<dyn SharedLogger>> = Vec::new();
    loggers.push(match (config.debug, config.verbose) {
        (_, true) => TermLogger::new(LevelFilter::Trace, simplelog::Config::default(), TerminalMode::Mixed),
        (true, _) => TermLogger::new(LevelFilter::Debug, simplelog::Config::default(), TerminalMode::Mixed),
        _ => TermLogger::new(LevelFilter::Info, simplelog::Config::default(), TerminalMode::Mixed)
    });
    CombinedLogger::init(loggers).expect("Unable to initialize loggers.");

    if config.debug {
        log::debug!("Configuration {:?}", config);
    }

    run(config).await;
}
