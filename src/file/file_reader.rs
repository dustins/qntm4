use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use async_std::sync::{Arc, Sender};
use async_std::task;
use async_trait::async_trait;
use bytes::BytesMut;

use crate::config::Config;
use crate::Runnable;

pub struct FileReader {
    config: Arc<Config>,
    tx: Sender<BytesMut>,
    path: PathBuf,
}

impl FileReader {
    pub fn new(config: Arc<Config>, tx: Sender<BytesMut>, path: PathBuf) -> Self {
        Self {
            config,
            tx,
            path,
        }
    }
}

#[async_trait]
impl Runnable for FileReader {
    async fn run(&mut self) {
        let packet_size = self.config.packet_size;
        let path = self.path.clone();
        let tx = self.tx.clone();

        task::spawn_blocking(move || {
            let mut file = File::open(path).expect("Unable to open file.");
            loop {
                let mut bytes = BytesMut::with_capacity(packet_size);
                unsafe { bytes.set_len(bytes.capacity()) }
                match file.read(&mut bytes) {
                    Ok(0) => break,
                    Ok(bytes_read) => {
                        unsafe { bytes.set_len(bytes_read) }
                        futures::executor::block_on(tx.send(bytes));
                    }
                    Err(e) => log::error!("{:?}", e)
                }
            }
        });
    }
}