use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

use async_std::sync::Arc;
use async_trait::async_trait;
use futures::StreamExt;

use crate::{Config, mux, Runnable};

pub struct FileWriter {
    config: Arc<Config>,
    stream: mux::Stream,
    path: PathBuf,
}

impl FileWriter {
    pub fn new(config: Arc<Config>, stream: mux::Stream, path: PathBuf) -> Self {
        Self {
            config,
            stream,
            path,
        }
    }
}

#[async_trait]
impl Runnable for FileWriter {
    async fn run(&mut self) {
        let mut file = File::create(&self.path).expect("Unable to open file for writing.");
        loop {
            match self.stream.next().await {
                None => break,
                Some(bytes) => {
                    match file.write(&bytes) {
                        Ok(_) => {}
                        Err(e) => log::error!("Unable to write to file. {:?}", e)
                    }
                }
            }
        }
    }
}