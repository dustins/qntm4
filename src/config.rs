use std::error::Error;
use std::path::PathBuf;

use async_std::net::SocketAddr;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Config {
    /// Activate debug logging
    #[structopt(short, long)]
    pub debug: bool,

    /// Activate verbose logging
    #[structopt(short, long)]
    pub verbose: bool,

    /// Size of packets to use
    #[structopt(long, default_value = "8192")]
    pub packet_size: usize,

    /// Activate FEC (Forward Error Correction) for input
    #[structopt(long)]
    pub fec_in: bool,

    /// Activate FEC (Forward Error Correction) for output
    #[structopt(long)]
    pub fec_out: bool,

    /// Activate de-multiplexing for input
    #[structopt(long)]
    pub mux_in: bool,

    /// Activate multiplexing for output
    #[structopt(long)]
    pub mux_out: bool,

    /// Number of data shards
    #[structopt(long, default_value = "10")]
    pub data_shards: usize,

    /// Number of parity shards
    #[structopt(long, default_value = "2")]
    pub parity_shards: usize,

    /// How many tasks to use for reading
    #[structopt(long, default_value = "1")]
    pub reader_tasks: usize,

    /// How many tasks to use for writing
    #[structopt(long, default_value = "1")]
    pub writer_tasks: usize,

    /// Source to read from (example: `file:someFile.txt`, `tcp:192.168.0.2:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "SOURCE", parse(try_from_str = parse_target))]
    pub source: Target,

    /// Destination to send to (example: `file:someFile.txt`, `tcp:192.168.0.2:4488`, `udp:192.255.255.255:4488`)
    #[structopt(name = "DESTINATION", parse(try_from_str = parse_target))]
    pub destination: Target,
}

impl Config {
    pub fn shards(&self) -> usize {
        self.data_shards + self.parity_shards
    }
}

#[derive(Debug)]
pub enum Target {
    Bench(usize),
    File(PathBuf),
    UDP(SocketAddr),
    TCP(SocketAddr),
}

fn parse_target(target: &str) -> Result<Target, Box<dyn Error>> {
    let delimiter = match target.find(":") {
        Some(index) => {
            index
        }
        None => return Err(Box::<dyn Error>::from("bad format"))
    };

    let (protocol, path) = target.split_at(delimiter);
    match protocol {
        "bench" => {
            Ok(Target::Bench(path[1..].parse::<usize>().unwrap()))
        }
        "tcp" => {
            Ok(Target::TCP(path[1..].parse::<SocketAddr>().unwrap()))
        }
        "udp" => {
            Ok(Target::UDP(path[1..].parse::<SocketAddr>().unwrap()))
        }
        "file" => {
            Ok(Target::File(path[1..].parse::<PathBuf>().unwrap()))
        }
        _ => Err(Box::<dyn Error>::from(format!("Unable to parse `{}`", target)))
    }
}

#[cfg(test)]
mod tests {
    use crate::config::Target::Bench;

    use super::*;

    impl Config {
        pub fn source(&mut self, source: Target) {
            self.source = source;
        }
    }

    impl Default for Config {
        fn default() -> Self {
            Self {
                debug: false,
                verbose: false,
                packet_size: 8192,
                fec_in: false,
                fec_out: false,
                mux_in: false,
                mux_out: false,
                data_shards: 10,
                parity_shards: 2,
                reader_tasks: 2,
                writer_tasks: 2,
                source: Bench(1024 * 1024),
                destination: Bench(0),
            }
        }
    }
}