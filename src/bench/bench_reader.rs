use async_std::sync::{Arc, Sender};
use async_trait::async_trait;
use bytes::BytesMut;

use crate::config::Config;
use crate::Runnable;

pub struct BenchReader {
    config: Arc<Config>,
    tx: Sender<BytesMut>,
    total_bytes: usize,
}

impl BenchReader {
    pub fn new(config: Arc<Config>, tx: Sender<BytesMut>, total_bytes: usize) -> Self {
        Self {
            config,
            tx,
            total_bytes,
        }
    }
}

#[async_trait]
impl Runnable for BenchReader {
    async fn run(&mut self) {
        let packet_size = self.config.packet_size;
        let mut bytes_remaining = self.total_bytes;
        loop {
            let mut bytes = BytesMut::with_capacity(packet_size);
            match bytes_remaining {
                remaining if remaining > packet_size => {
                    unsafe { bytes.set_len(packet_size) }
                    self.tx.send(bytes).await;
                    bytes_remaining -= packet_size;
                }
                remaining if remaining > 0 => {
                    unsafe { bytes.set_len(remaining) }
                    self.tx.send(bytes).await;
                    bytes_remaining -= remaining;
                }
                _ => break,
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use async_std::{sync::channel, task};

    use crate::Target;

    use super::*;

    #[async_std::test]
    async fn byte_total() {
        let (tx, rx) = channel(2);
        let mut config = Config::default();
        let total_bytes = 1024 * 1024 * 1024;
        config.source(Target::Bench(total_bytes));
        let mut runner = BenchReader::new(Arc::new(config), tx, total_bytes);

        task::spawn(async move {
            runner.run().await;
        });

        let mut received_bytes = 0;
        while let Ok(bytes) = rx.recv().await {
            received_bytes += bytes.len();
        }
        assert_eq!(total_bytes, received_bytes);
    }
}