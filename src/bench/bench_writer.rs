use async_std::sync::Arc;
use async_trait::async_trait;
use futures::StreamExt;

use crate::{Config, mux, Runnable};

pub struct BenchWriter {
    config: Arc<Config>,
    stream: mux::Stream,
    total_bytes: usize,
}

impl BenchWriter {
    pub fn new(config: Arc<Config>, stream: mux::Stream, total_bytes: usize) -> Self {
        Self {
            config,
            stream,
            total_bytes,
        }
    }
}

#[async_trait]
impl Runnable for BenchWriter {
    async fn run(&mut self) {
        let mut total_bytes = 0;

        while let Some(output) = self.stream.next().await {
            total_bytes += output.len();
        }

        if self.config.debug {
            log::debug!("[Stream#{}] Sent {} bytes", self.stream.id(), total_bytes);
        }
    }
}