use std::path::PathBuf;

use async_std::net::TcpListener;
use async_std::sync::{Arc, channel, Receiver, Sender};
use async_std::task;
use async_trait::async_trait;
use bytes::BytesMut;
use futures::AsyncReadExt;
use nohash_hasher::IntMap;

use crate::{Config, FileReader, mux, Runnable, StreamId, Target};
use crate::bench::BenchReader;
use crate::udp_tunnel::UdpReader;

pub struct Reader {
    config: Arc<Config>,

    tx_bytes: Sender<BytesMut>,
    rx_bytes: Receiver<BytesMut>,

    tx_stream: Sender<mux::Stream>,
    rx_stream: Receiver<mux::Stream>,

    streams: IntMap<StreamId, mux::Stream>,
}

impl Reader {
    pub fn new(config: Arc<Config>) -> Self {
        let (tx_bytes, rx_bytes) = channel(32);
        let (tx_stream, rx_stream) = channel(32);

        Self {
            config,

            tx_bytes,
            rx_bytes,

            tx_stream,
            rx_stream,

            streams: IntMap::default(),
        }
    }

    pub fn output(&self) -> Receiver<mux::Stream> {
        self.rx_stream.clone()
    }
}

#[async_trait]
impl Runnable for Reader {
    async fn run(&mut self) {
        let tx_bytes = self.tx_bytes.clone();
        let rx_bytes = self.rx_bytes.clone();
        let tx_stream = self.tx_stream.clone();
        let config = self.config.clone();

        // todo redirect tx for demux
        task::spawn(Reader::demux(rx_bytes, tx_stream));

        // todo redirect tx for fecdecode

        // todo redirect tx for organizing

        match &self.config.source {
            Target::Bench(total_bytes) => Reader::run_bench(config, tx_bytes, total_bytes).await,
            Target::File(path) => Reader::run_file(config, tx_bytes, path.clone()).await,
            Target::UDP(address) => {
                let mut runner = UdpReader::new(tx_bytes, address.clone(), 1);
                task::spawn(async move {
                    runner.run().await;
                });
            }
            Target::TCP(address) => {
                let config = self.config.clone();
                let address = address.clone();
                task::spawn(async move {
                    let listener = TcpListener::bind(address).await.expect(&format!("Unable to bind to {:?}", address));
                    while let Ok((mut connection, _)) = listener.accept().await {
                        let config = config.clone();
                        let tx_bytes = tx_bytes.clone();
                        task::spawn(async move {
                            let stream_id = mux::Stream::next_id();
                            tx_bytes.send(mux::Frame::open(stream_id).encode()).await;

                            loop {
                                let mut bytes = BytesMut::with_capacity(config.packet_size);
                                unsafe { bytes.set_len(bytes.capacity()) }
                                match connection.read(&mut bytes).await {
                                    Ok(0) => break,
                                    Ok(bytes_read) => {
                                        unsafe { bytes.set_len(bytes_read) }
                                        tx_bytes.send(mux::Frame::push(stream_id, bytes).encode()).await;
                                    }
                                    Err(e) => panic!(e)
                                }
                            }

                            tx_bytes.send(mux::Frame::close(stream_id).encode()).await;
                        });
                    }
                });
            }
        };
    }
}


impl Reader {
    async fn demux(rx: Receiver<BytesMut>, tx_stream: Sender<mux::Stream>) {
        let mut streams: IntMap<StreamId, Sender<BytesMut>> = IntMap::default();

        while let Ok(bytes) = rx.recv().await {
            let frame = mux::Frame::decode(bytes);
            let frame_id = frame.id();
            match frame.command() {
                mux::Command::OPEN => {
                    let (stream_tx, stream_rx) = channel(32);
                    streams.insert(frame_id, stream_tx);
                    let stream = mux::Stream::new(frame_id, stream_rx);
                    tx_stream.send(stream).await;
                }
                mux::Command::PUSH => {
                    match streams.get_mut(&frame_id) {
                        Some(sender) => sender.send(frame.body().expect("Expected Frame to have body.")).await,
                        _ => log::warn!("Trying to push data for stream {} but no such stream exists", frame_id),
                    };
                }
                mux::Command::NOOP => {}
                mux::Command::CLOSE => {
                    match streams.remove(&frame_id) {
                        Some(_) => log::debug!("Removed stream {}", frame_id),
                        _ => log::warn!("Tried to remove stream {} but no such stream exists", frame_id),
                    }
                }
            }
        }
    }
}

impl Reader {
    async fn run_bench(config: Arc<Config>, tx_bytes: Sender<BytesMut>, total_bytes: &usize) {
        let (frame_tx, frame_rx) = channel(32);

        // convert bytes into encoded mux::Frame
        let muxer = task::spawn(async move {
            let stream_id = mux::Stream::next_id();
            tx_bytes.send(mux::Frame::open(stream_id).encode()).await;
            while let Ok(bytes) = frame_rx.recv().await {
                tx_bytes.send(mux::Frame::push(stream_id, bytes).encode()).await;
            }
            tx_bytes.send(mux::Frame::close(stream_id).encode()).await;
        });

        let mut receiver = BenchReader::new(config, frame_tx, *total_bytes);
        let runner = task::spawn(async move {
            receiver.run().await;
        });

        futures::future::join(muxer, runner).await;
    }

    async fn run_file(config: Arc<Config>, tx_bytes: Sender<BytesMut>, path: PathBuf) {
        // convert bytes into encoded mux::Frame
        let frame_tx = match config.mux_in {
            true => tx_bytes,
            false => {
                let (frame_tx, frame_rx) = channel(32);

                task::spawn(async move {
                    let stream_id = mux::Stream::next_id();
                    tx_bytes.send(mux::Frame::open(stream_id).encode()).await;
                    while let Ok(bytes) = frame_rx.recv().await {
                        tx_bytes.send(mux::Frame::push(stream_id, bytes).encode()).await;
                    }
                    tx_bytes.send(mux::Frame::close(stream_id).encode()).await;
                });

                frame_tx
            }
        };

        let mut receiver = FileReader::new(config, frame_tx, path);
        let runner = task::spawn(async move {
            receiver.run().await;
        });

        // todo validate that we don't need to wait for the muxing task to finish too
        runner.await;
        // futures::future::join(muxer, runner).await;
    }
}
